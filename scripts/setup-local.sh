#!/bin/sh
set -eou

success_banner () {
    echo " "
    echo "#####################################"
    echo "# Wordpress is now ready to go!     #"
    echo "#                                   #"
    echo "# Wordpress:  http://localhost      #"
    echo "# MailHog:    http://localhost:8025 #"
    echo "# PhpMyAdmin: http://localhost:8080 #"
    echo "#####################################"
}

# Wait for WP to initialize
until [ -f /var/www/html/wp-config.php ]
do
    echo "Waiting for WP to initialize..."
    sleep 2
done

# Wait for DB to initialize
while ! mysqladmin ping -h"db" -u $WORDPRESS_DB_NAME -p$WORDPRESS_DB_PASSWORD --silent; do
    echo "Waiting for DB to initialize..."
    sleep 2
done

if wp core is-installed; then
    sleep 3
    success_banner
    return 0
fi

activate_plugin () {
    until [ -d "/var/www/html/wp-content/plugins/$1" ]
    do
        echo "Waiting for $1 to be installed..."
        sleep 2
    done
    wp plugin activate $1
}

wp core install --url="localhost" --title="$WP_TITLE" --admin_user="timelab" --admin_password="dodgethis" --admin_email="drift@timelab.se" --skip-email
wp option set blog_public 0
wp rewrite structure '/%postname%/'

echo "Deleting all comments.."
wp comment delete $(wp comment list --format=ids)
echo "Deleting all posts.."
wp post delete $(wp post list --format=ids)

activate_plugin w3-total-cache
activate_plugin amazon-s3-and-cloudfront
activate_plugin wp-migrate-db-pro
activate_plugin wp-migrate-db-pro-cli
activate_plugin wp-migrate-db-pro-media-files
activate_plugin wordpress-seo
wp option add wp_mail_smtp_activation_prevent_redirect true
activate_plugin wp-mail-smtp

echo "Installing language files.."
wp language plugin install --all sv_SE
wp language core install sv_SE
wp site switch-language sv

success_banner
