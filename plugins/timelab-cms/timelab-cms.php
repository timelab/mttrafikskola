<?php
/*
Plugin Name: Timelab CMS
Plugin URI: #
Description:
Author: Timelab
Version: 2.0.0
Author URI: http://timelab.se
Text Domain: timelab-cms
*/

require_once('src/cms_module.php');
require_once('src/old_modules/admin.php');

require_once('src/Cms.php');

$old_cms = new admin();
$cms = new \Timelab\Cms\Cms();
