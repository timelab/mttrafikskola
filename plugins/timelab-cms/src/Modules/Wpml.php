<?php
/**
 * Created by PhpStorm.
 * User: patrik
 * Date: 9/18/14
 * Time: 6:14 PM
 */

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;

require_once('WpmlApi.php');

/**
 * Adds WPML integration to all modules with multi-lingual support. NOTE! Only enable **AFTER** WPML is **ACTIVATED** or there will be dragons.
 *
 * Class Wpml
 * @package Timelab\Cms\Modules
 */
class Wpml extends ModuleAbstract {

    private $api = null;

    public function __construct()
    {
        $this->api = new WpmlApi();
        parent::__construct();
    }


    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return true;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        // TODO: Implement jsFiles() method.
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {

    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        // TODO: Implement getDependencies() method.
    }

    /**
     * Adds the module to the WordPress menu and links it to the route() function of the module
     */
    public function addToMenu()
    {
        return null; // Don't add this to the menu
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        // TODO: Implement getMenuOrder() method.
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        // TODO: Implement earlyExecute() method.
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        // TODO: Implement execute() method.
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        return $this->api;
    }

} 