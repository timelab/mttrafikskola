<div class="wrap container mainText PodsBrands" role="document">
	<div class="row relative">
		<div class="subHeader">
			<div class="marginFixSub">
				<div class="page-header">
					<h1>Sidan hittades ej!</h1>
				</div>
			</div>
		</div>

		<div class="marginFixSub">
			<div class="row">

				<div class="col-md-8 subContent subBorderRight">
					Hoppsan! Sidan du försökte nå finns ej, navigera vidare med hjälp av menyn ovan.
				</div>

				<div class="col-md-4 ">
					<?php echo emitShowcases_vertical($cms);//exists in t1-lib.php ?>
				</div>
			</div>
		</div>
	</div>
</div>
