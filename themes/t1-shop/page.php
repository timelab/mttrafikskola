<div class="wrap container mainText PodsBrands subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>
	</div>

	<div class="row relative minPageHeight">
		<div class="col-sm-9 col-md-8 subContent">
			<?php get_template_part('templates/content', 'page'); ?>
		</div>

		<div class="col-sm-3 col-md-4 subBorderLeft shop-sidebar">
			<div class="view-btn"><i class="fa fa-spinner fa-spin"></i></div>
			<div class="row showcases collapsed">
				<div class="col-sm-12 col-md-11 col-md-offset-1 shop-cat-menu">
					<ul class="widget-ul">
						<?php
						dynamic_sidebar('Shop Sidebar');
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
