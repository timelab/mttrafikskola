<?php
global $cms;
global $t1config;
?>
<div class="facebook socialPane">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/sv_SE/sdk.js#xfbml=1&version=v2.4&appId=1442914462598705";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div class="fb-page" data-href="<?php echo $t1config['href_facebook']; ?>" data-tabs="timeline" data-width="500" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $t1config['href_facebook']; ?>"><a href="<?php echo $t1config['href_facebook']; ?>">MT Trafikskola AB Kalix &amp; Luleå</a></blockquote></div></div>

    <div><a class="socialClose" href="#">&#xf057;</a></div>
</div>



<div class="row top-header">
  <a href="/" target="_self"><div class="col-xs-10 col-sm-5 col-md-5 col-lg-5 logo-mobile">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/site-logo.png" alt="Logotype" class="topLogo"/>
  </div></a>

 <div class="col-xs-2 col-sm-7 col-md-7 col-lg-7">
      <?php
      // Social networking icons.
      //--------------------------------------------------------------------


      $social = emitSocial($cms);

      if (!empty($social))
      {

        echo "<div class='social'>";

        global $woocommerce;
        ?>

          <div class="bokaknapp hidden-xs"><a href="http://www.timecenter.com/mttrafikskola/" target="_blank"><span style="color: #000000;"><img style="border: 0px; margin-bottom: 10px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/boka-tid.png" alt="Boka tid"></span></a></div>
          <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('Visa kundkorg', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo $woocommerce->cart->cart_contents_count;?></span></a>
          <?php

        echo "</div>";

      }

      //--------------------------------------------------------------------

      // Contact info.
      //--------------------------------------------------------------------
      $contactInfo = '';
      $repArr = array ("(0)", "-", " "); // array för att fixa till telefonnr i länkar.

      // Build HTML representation.
      $facilities = getContactInfo($cms);

      if ($t1config['header_type'] === HEADER_TYPE_1)
      {
        $contactInfo .= "<div class='item'>";
        foreach ($facilities as $facility)
        {
          if (isset($facility['email']))
          {
            $contactInfo .= "<div class='row topinfo-item'>";
            $contactInfo .= "<div class='col-xs-12'>";
            $contactInfo .= "<span class='glyphicon glyphicon-envelope'></span>";
            $contactInfo .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
            $contactInfo .= "</div>";
            $contactInfo .= "</div>";
          }
          if (isset($facility['telephone']))
          {
            $contactInfo .= "<div class='row topinfo-item'>";
            $contactInfo .= "<div class='col-xs-12'>";
            $contactInfo .= "<span class='glyphicon glyphicon-phone'></span>";
            $contactInfo .= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
            $contactInfo .= "</div>";
            $contactInfo .= "</div>";
          }
        }
        $contactInfo .= "</div>";
      }

      echo "<div class='top-info hidden-xs'>";
      echo $contactInfo;
      echo "</div>";
      //--------------------------------------------------------------------
      ?>
  </div>

</div>