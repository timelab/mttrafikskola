<?php
global $cms;
global $t1config;
$footer = '';
$repArr = array ("(0)", "-", " "); // array för att fixa till telefonnr i länkar.

// Bulid HTML representation.
$facilities = getContactInfo($cms);
if ($t1config['footer_type'] === FOOTER_TYPE_1)
{
    $footer .= "<div class='item'>";
    foreach ($facilities as $facility)
    {
        // All inside panel
        $footer .= '<div class="text">';
        $footer .= "<span class='glyphicon glyphicon-home'></span>";
        $footer .= $facility['address'];
        $footer .= '</div>';

        if (isset($facility['email']))
        {
            $footer .= "<div class='h-separator'></div>";
            $footer .= "<div class='text'>";
            $footer .= "<span class='glyphicon glyphicon-envelope'></span>";
            $footer .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
            $footer .= "</div>";
        }
        if (isset($facility['telephone']))
        {
            $footer .= "<div class='h-separator'></div>";
            $footer .= "<div class='text'>";
            $footer .= "<span class='glyphicon glyphicon-earphone'></span>";
            $footer	.= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
            $footer .= "</div>";
        }
    }
    $footer .= "</div>";
}
if ($t1config['footer_type'] === FOOTER_TYPE_2)
{
    $facility = $facilities[0];

    $footer .= "<div class='hidden-xs col-sm-6 col-md-3 col-lg-3 item logo'>";
    $footer .= "<img src='" . get_template_directory_uri() . "/assets/img/logo_footer.png' alt='Logotype' class='logo'/>";
    $footer .= "</div>";

    $footer .= "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3 item text'>";
    $footer .= "<span>Besöksadress</span>";
    $footer .= $facility['address_2'];
    $footer .= "</div>";

    if (isset($facility['telephone']))
    {
        $footer .= "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3 item text'>";
        $footer .= "<span>Telefon</span>";
        $footer	.= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
        $footer .= "</div>";
    }
    if (isset($facility['email']))
    {
        $footer .= "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3 item text'>";
        $footer .= "<span>E-post</span>";
        $footer .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
        $footer .= "</div>";
    }
}

if ($t1config['footer_type'] === FOOTER_TYPE_3) {
    $facility = $facilities[0]; //robins
    $podparams = array('limit' == 1);
    $footerpod = pods('sidfot_info', $podparams);
    $footerHeader = '';
    $footerText = '';
    while ($footerpod->fetch()) {
        $footerHeader = $footerpod->field('post_title');
        $footerText = $footerpod->display('post_content');
    }

    /** @var Timelab\Cms\Objects\Facility[] $facilities */
    $facilities_detail = $cms->getApi('Contact')->getFacilities();

    $facility_detail = $facilities_detail[0];
    $address = $facility_detail->getAddress();

    $footer .= "<div class='background'><img src='" . get_template_directory_uri() . "/assets/img/footer_bg.png'></div>";

    $footer .= "<div class='row'>";
    //texten
    $footer .= "<div class='col-sm-12 col-md-6 col-lg-6'>";
    $footer .= "<div class='kontaktHeadline'>" . $footerHeader . "</div>";

    $footer .= "<div class='kontaktText'>" . $footerText . "</div>";
    $footer .= "</div>";

    //addressen
    $footer .= "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>";
    $footer .= "<div class='kontaktHeadline'>Kontaktinformation</div>";
    $footer .= "<div class='kontakt underline'>" . $address->getStreet() . "</div>";
    $footer .= "<div class='kontakt underline'>" . $address->getZip() . " " . $address->getCity() . " </div>";
    $footer .= "<div class='kontakt underlineMobile'>Sverige</div>";
    $footer .= "</div>";


    //kontaktuppgifterna
    $footer .= "<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>";
    $footer .= "<div class='kontaktHeadline kontaktHeadlineMobileHide'> </div>";

    if (isset($facility['telephone'])) {
        $footer .= "<div class='kontakt underline'>";
        $footer .= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
        $footer .= "</div>";
    }
    if (isset($facility['mobile'])) {
        $footer .= "<div class='kontakt underline'>";
        $footer .= "<a href='tel:" . str_replace($repArr, '', $facility['mobile']) . "'>{$facility['mobile']}</a>";
        $footer .= "</div>";
    }
    if (isset($facility['email'])) {
        $footer .= "<div class='kontakt'>";
        $footer .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
        $footer .= "</div>";
    }



    $footer .= "</div>";
    $footer .= "</div>";
    $footer .= "<div class='row'>";
    $footer .= "<div class='col-xs-12 bottomLogo'>";
    $footer .= "<a href='/' target='_self'><img src='". get_template_directory_uri() ."/assets/img/footerlogo2.png'></a>";
    $footer .= "</div>";
    $footer .= "</div>";
}
?>


    <footer class="content-info container footer <?php echo $t1config['footer_type']; ?>" role="contentinfo">
        <div class="footer-container">
            <?php echo $footer; ?>
        </div>
    </footer>

<?php wp_footer(); ?>