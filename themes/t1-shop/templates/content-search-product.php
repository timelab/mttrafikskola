<article <?php post_class(); ?>>
  <header>
    <h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> (Produkt)</a></h4>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article> 