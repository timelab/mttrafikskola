<div class="row">
    <header class="banner navbar navbar-inverse" role="banner">
        <div class="icon hidden-xs facebookBtn"></div>
        <a href="https://www.facebook.com/MTTrafikskolaAB"><div class="icon hidden-lg hidden-md hidden-sm facebookBtn2"></div></a>
        <div class="navbar-header">
            <div class="bokaknapp2 hidden-lg hidden-md hidden-sm"><a href="http://www.timecenter.com/mttrafikskola/" target="_blank"><span style="color: #000000;"><img style="border: 0px; margin-bottom: 10px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/boka-tid.png" alt="Boka tid"></span></a></div> <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>
        </div>

        <nav class="collapse navbar-collapse" role="navigation">
            <?php
            if (has_nav_menu('primary_navigation'))
            {
                wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
            }
            ?>
        </nav>
    </header>
</div>