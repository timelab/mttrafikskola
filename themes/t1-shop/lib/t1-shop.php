<?php
/* Woocommerce specific filters / changes */

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-lightbox' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['order']['order_comments']['placeholder'] = 'Fyll i personnummer, namn och adress';
     $fields['order']['order_comments']['label'] = 'Fakturauppgifter om annan än elevens';
     return $fields;
}



add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category() ){
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if ( $image ) {
            echo '<img src="' . $image . '" alt="" />';
        }
    }
}


/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_before_checkout_billing_form', 'my_custom_checkout_field' );
function my_custom_checkout_field( $checkout ) {

    $user = wp_get_current_user();
    $social_security = '';
    if($user && $user->ID > 0){
        $social_security = get_the_author_meta('social_security',$user->get('ID'));
    }

    echo '<div class="info-notice"><i class="fa fa-exclamation-circle"></i> Obs! Alla uppgifter nedan måste fyllas i med elevens uppgifter.</div>';

    echo '<div id="my_custom_checkout_field">';

    woocommerce_form_field( 'social_security', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'required'      => true,
        'label'         => __('Personnummer'),
        'placeholder'   => __('ÅÅÅÅMMDD-NNNN'),
    ), $social_security);

    echo '</div>';

}

/**
 * Process the checkout
 */
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['social_security'] )
        wc_add_notice( __( 'Personnummer måste anges.' ), 'error' );
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['social_security'] ) ) {
        update_post_meta( $order_id, 'social_security', sanitize_text_field( $_POST['social_security'] ) );

        $newuser = wp_get_current_user();
        if($newuser && $newuser->ID > 0){
            update_user_meta($newuser->get('ID'),'social_security',sanitize_text_field( $_POST['social_security'] ));
        }
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){

    if($order->user_id !== null && $order->user_id > 0){
        echo '<div style="clear:both"><p><strong>'.__('Personnummer').':</strong> ' . get_the_author_meta('social_security',$order->user_id) . '</p></div>';
    } else {
        echo '<div style="clear:both"><p><strong>'.__('Personnummer').':</strong> ' . get_post_meta( $order->id, 'social_security', true ) . '</p></div>';
    }
}


add_action('show_user_profile', 'my_add_extra_profile_fields');
add_action( 'edit_user_profile', 'my_add_extra_profile_fields' );
function my_add_extra_profile_fields($user)
{
    ?>
        <h3>Extra info</h3>
        <table class="form-table">

            <tr>
                <th><label for="social_security">Personnummer</label></th>
                <td><input type="text" name="social_security" id="social_security" value="<?php echo esc_attr(get_the_author_meta( 'social_security', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
        </table>
    <?php

}

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
function my_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;


    update_user_meta($user_id,'social_security', $_POST['social_security']);


}

/* Dibs kräver 'SEK' istället för 'kr' */
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
	//var_dump($currency);
     switch( $currency ) {
          case 'SEK': $currency_symbol = 'SEK'; break;
     }
     return $currency_symbol;
}






/*
 * Shipment tracking
 */

add_filter( 'wc_shipment_tracking_get_providers', 'shipping_providers');

function shipping_providers() {
	    return array(
        "Sverige" => array(
            'Posten AB'     => 'http://www.posten.se/sv/Kundservice/Sidor/Sok-brev-paket.aspx?search=%1$s',
            'DHL.se'        => 'http://www.dhl.se/content/se/sv/express/godssoekning.shtml?brand=DHL&AWB=%1$s',
            'Bring.se'      => 'http://tracking.bring.se/tracking.html?q=%1$s',
            'UPS.se'        => 'http://wwwapps.ups.com/WebTracking/track?track=yes&loc=sv_SE&trackNums=%1$s',
            'DB Schenker'   => 'http://privpakportal.schenker.nu/TrackAndTrace/packagesearch.aspx?packageId=%1$s'
        )
    );
}





/*
* Byta av bild saknas
*
**/
add_action( 'init', 'custom_fix_thumbnail' );

function custom_fix_thumbnail() {
	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

	function custom_woocommerce_placeholder_img_src( $src ) {
		$src = get_template_directory_uri().'/assets/img/placeholder.png';
		return $src;
	}
}



//Products per page, 12 as standard
$productsPerPage = isset($_GET["perpage"]) ? ($_GET["perpage"] == 'alla' ? -1 : $_GET["perpage"]) : 12;
add_filter( 'loop_shop_per_page', function($cols) use ($productsPerPage) { return $productsPerPage; }, 20 );


//New priceformat for variable products with "From 30 sek" insted of "30 sek to 60 sek"
add_filter( 'woocommerce_variable_sale_price_html', 'wc_wc20_variation_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2 );
function wc_wc20_variation_price_format( $price, $product ) {

	$max_price = $product->get_variation_price( 'max', true );
	$min_price = $product->get_variation_price( 'min', true );
	$sale_price_min = $product->get_variation_sale_price('min',true);

	if($sale_price_min == false){
		if($max_price != $min_price){
			$price = '<ins>'.sprintf( __( 'Från: %1$s', 'woocommerce' ), wc_price( $min_price ) ).'</ins>';
		}else{
			$price = '<ins>'.wc_price( $min_price ).'</ins>';
		}
	}else {
		$regular_price_min = $product->get_variation_regular_price('min',true);
		if($max_price != $min_price){
			$price = '<del>'.wc_price( $regular_price_min ).'</del><ins>'.sprintf( __( ' Från: %1$s', 'woocommerce' ), wc_price( $sale_price_min ) ).'</ins>';
		}else{
			$price = '<del>'.wc_price( $regular_price_min ).'</del><ins>'.wc_price( $sale_price_min ).'</ins>';
		}
	}

	return $price;
}


// Ajax update for the cart-link, must be exactly the same HTML as the one in base.php
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	get_the_ID();
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('Visa kundkorg', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo $woocommerce->cart->cart_contents_count; ?></span></a>
	<?php
	$fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}


//New widget area for the webshop
register_sidebar(
	array (
		'name' => 'Shop Sidebar'
	)
);


//Read more link whereever it is used
function new_excerpt_more( $more ) {
	return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Läs mer</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


function woo_related_products_limit() {
	global $product;

	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 4 columns
	return $args;
}

function woocommerce_store_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0) {
	global $post;
	$product = wc_get_product($post->ID);

	$images = $product->get_gallery_attachment_ids();

	if (!empty($images)) {
		echo wp_get_attachment_image( $images[0], apply_filters( $size, 'shop_catalog' ) );
	} else {
		// PLACEHOLDER HÄR
	}


}

// TEST SHIT
/*remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_store_thumbnail', 10);*/



?>
