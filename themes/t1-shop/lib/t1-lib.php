<?php

$baseUri = get_template_directory_uri();

function emitBrands($cms)
{
	global $t1config;
	$html = "";

	if ($t1config['brands_enabled'] === false)
	{
		return $html;
	}

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 6);
	$objectList = pods('brands', $podparams);
	if ($objectList->total() > 0)
	{
		$html .= "<div class='container showcases brands PodsPuffar {$t1config['hide_brands']}'>";
		$html .= "<div class='row'>";

		$classes = "";
		switch ($objectList->total())
		{
			case 1:
			case 2:
			case 3:
				$classes = "col-xs-4 col-sm-4 col-md-4";
				break;
			case 4:
				$classes = "col-xs-6 col-sm-3 col-md-3";
				break;
			case 5:
			case 6:
				$classes = "col-xs-6 col-sm-2 col-md-2";
				break;
			default:
				$classes = "col-xs-6 col-sm-2 col-md-2";
		}
		$border = "border";

		$i = 0;
		while ($objectList->fetch())
		{
			$i++;
			if ($i === $objectList->total()) { $border = ""; }

			$linkArr = $objectList->field('puff_lank');

			$html .= ($linkArr !== "") ? "<a href='{$linkArr}'>" : "";
			$html .= "<span class='{$classes} {$border} brand'>";
			$html .= "<span class='brand-bg'>";
			$html .= '<img src="' . $objectList->display('puff_bild') . '"/>';

			if (!empty($objectList->display('puff_bild_hover')))
			{
				$html .= '<img class="hover" src="' . $objectList->display('puff_bild_hover') . '"/>';
			}
			$html .= '</span>';
			$html .= '</span>';
			$html .= ($linkArr !== "") ? '</a>' : '';
		}
		$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}


function emitShowcases($cms)
{
	$html = "";

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);
	if ($objectList->total() > 0)
	{
		$count = 0;

		$html .= "<div class='container showcases PodsPuffar'>";
		$html .= "<div class='row showcase-row'>";
		while ($objectList->fetch())
		{
			$count++;

			if($linkArr = $objectList->field('extern_lank') != ''){
				$podpermalink = $objectList->field('extern_lank');
			}else{
				$linkArr = $objectList->field('puff_lank');
				$podpermalink = get_permalink($linkArr['ID']);
			}


			$html .= "<a href='$podpermalink'>";
			$html .= "<span class='col-md-3 col-sm-6 col-xs-6 showcase pos$count'>";
			$html .= "<span class='showcase-bg'>";
			$html .= "<img src='{$objectList->display('puff_bild')}' alt='{$objectList->field('puff_text')}'/>";
			if (!empty($objectList->display('puff_bild_hover')))
			{
				$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
			}
			$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
			if ($objectList->field('puff_text') !== "")
			{
				$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
			}
			$html .= "</span>";
			$html . "</span>";
			$html .= "</a>";
		}
		$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}


function emitShowcases_vertical($cms)
{
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);

	$html = '<div class="showcases showcases-vertical">';
	if ($objectList->total() > 0)
	{
		// loop through items using pods::fetch
		$count = 0;
		while ($objectList->fetch())
		{
			$count++;
			if($linkArr = $objectList->field('extern_lank') != ''){
				$podpermalink = $objectList->field('extern_lank');
			}else{
				$linkArr = $objectList->field('puff_lank');
				$podpermalink = get_permalink($linkArr['ID']);
			}

			$html .= "<div class='col-xs-6 col-sm-6 col-md-12 showcase showcase-vertical pos$count'>";
			$html .= "<a href='$podpermalink' class='showcase-href'>";
			$html .= '<span class="showcase-bg">';
			$html .= '<img src="'.$objectList->display('puff_bild').'" alt="'.$objectList->field('puff_text').'" />';
			if (!empty($objectList->display('puff_bild_hover')))
			{
				$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
			}
			$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
			if ($objectList->field('puff_text') !== "")
			{
				$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
			}
			$html .= '</span>';
			$html .='</a>';
			$html .= '</div>';
		}
	}
	$html .='</div>';
	return $html;
}


function emitSlider($cms, $sliderID)
{
	$html = "<div class='sliderShadow'></div>";
	$html .= "<div class='flexslider'>";
	$html .= "<ul class='slides'>";

	/** @var $slider Timelab\Cms\Objects\SliderObject */
	$slider = $cms->getApi('Slider')->getSlider($sliderID);

	foreach ($slider->getImages(true) as $slide)
	{
		$title      = $slide->getTitle();
		$subtitle   = $slide->getSubtitle();
		$text       = $slide->getText();

		$slideHtml = "";

		if ( !(empty($title) && empty($subtitle) && empty($text))  )
		{
			$slideHtml .= "<div class='slideText'>";

			$href       = $slide->getHref();

			$slideHtml .= empty($title) ? "" : "<h2>{$title}</h2>";
			$slideHtml .= empty($subtitle) ? "" : "<h3>{$subtitle}</h3>";
			$slideHtml .= empty($text) ? "" : "<p>{$text}</p>";
			$slideHtml .= "</div>";
		}


		$slideHtml .= "<img src='{$slide->getSrc()}'/>";

		$html .=  "<li>";
		$html .= empty($href) ? $slideHtml : "<a href='{$href}' target='{$slide->getTarget()}'>$slideHtml</a>";
		$html .= "</li>";
	}
	$html .= "</ul>";
	$html .= "</div>";

	return $html;
}


function emitSocial($cms)
{
	global $t1config;
	$html = "";

	if ($t1config['social_integration_enabled'] === false)
	{
		return $html;
	}

	$html .= "<div class='hidden-xs'>";

    $html .= "<a href='#' class='{$t1config['facebook_icon']} facebookBtn'>";
    $html .= "<img src='" . get_template_directory_uri() . "/assets/img/social_facebook_{$t1config['facebook_icon']}.png' alt='Facebook'/>";
    $html .= "</a>";

	if (!empty($t1config['href_twitter']))
	{
		$html .= "<a href='{$t1config['href_twitter']}' target='_blank'>";
		$html .= "<img src='" . get_template_directory_uri() . "/assets/img/twitter.png' alt='Twitter'/>";
		$html .= "</a>";
	}


    $html .= "<a href='#' class='{$t1config['facebook_icon']} instagramBtn'>";
    $html .= "<img src='" . get_template_directory_uri() . "/assets/img/instagram.png' alt='Instagram'/>";
    $html .= "</a>";


	$html .= "</div>";

	return $html;
}


function getContactInfo($cms)
{
	// Loop through facilities to gather data.
	$facilities = array();
	foreach ($cms->getApi('Contact')->getFacilities() as $facility)
	{
		$address = $facility->getAddress();

		$data = array();
		$data['address'] = $address->getStreet() . ' ' . $address->getZip() . ' ' . $address->getCity();
		$data['address_2'] = $address->getStreet() . ' ' . $address->getCity();
		foreach ($facility->getContactDetails() as $fUppgift)
		{
			if ($fUppgift->getValue() !== '')
			{
				$data[$fUppgift->getType()] = $fUppgift->getValue();
			}
		}
		$facilities[] = $data;
	}

	return $facilities;
}