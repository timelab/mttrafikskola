<?php

$site_url = get_site_url();

//Hämta alla nyheter för sidebar och senaste ID för direkt visning
$date = date('Y-m-d H:i');
$podparams = array(
	'where' => "synlig.meta_value=True AND datum.meta_value < '{$date}' ",
	'orderby' => 'datum.meta_value DESC',
	'limit' => 50
);

$objectList = pods('nyheter',$podparams);
$latestNewsID = 0;
$sideBarHtml = '';
$newsIndex = 0;
$activeMenu = '';


if ($objectList->total() > 0 ) {
	while ($objectList->fetch() ) {

		//Ebart newsID för senaste (Först i listan)
		if($newsIndex<1){ $latestNewsID =  $objectList->field('slug'); }

		$podpermalink = get_permalink($linkArr['ID']);
		$sideBarHtml .= '<a class="LeftMenuItem" href="'.$podpermalink.$objectList->field('slug').'">';
		$sideBarHtml .=  $objectList->field('name');
		$sideBarHtml .=  '</a>';
		$newsIndex++;
	}
}

//Om enskild nyhet hämta id för aktuell nyhet, och hitta förälder i menyn
if(is_single()){
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			$latestNewsID = $post->post_name;

			//Hitta menyförälder då pods saknar föräldrar i menyn
			$activeMenu = str_replace(get_site_url().'/','',get_permalink( $post->ID));
			$activeMenu = str_replace("/",'',str_replace($latestNewsID,'',$activeMenu));

		}
	}
}

?>
<div class="wrap container mainText PodsBrands subpage" role="document">


	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>
	</div>

	<div class="row relative minPageHeight">
		<div class="col-md-8 subContent">
			<!-- skriv ut sidan -->
			<?php get_template_part('templates/content', 'page'); ?>

			<?php


			$object = pods(  'nyheter', $latestNewsID );
			$html .='<span class="">';
			//$html .='<h2 class="nyhetRubrik">'.$object->field('name').'</h2>';
			$html .='<span class="nyhetDatum">'.str_replace("00:00:00","", $object->field('datum') ).'</span>';
			$html .='<span class="nyhetText">'.$object->display('text').'</span>';
			$html .='</span>';



			echo $html;

			?>
		</div>

		<div class="col-md-4 subBorderLeft">
			<div class="nyheter nyheter_lista" >
				<h3>Nyheter</h3>
				<?php
					echo $sideBarHtml;
				?>
			</div>
		</div>
	</div>
</div>
<?php
if($activeMenu != ''){?>
<script type="text/javascript">
	$(function(){
		//Pods saknar förälder så vi sätter den manuellt
		$('.nav.navbar-nav li.menu-<?php echo $activeMenu; ?>').addClass('active');
	});
</script>
<?php }?>





