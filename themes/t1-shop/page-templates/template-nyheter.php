<?php
/*
Template Name: Nyheter
*/

$site_url = get_site_url();


$date = date('Y-m-d H:i');
$podparams = array(
    'where' => "synlig.meta_value=True AND datum.meta_value < '{$date}' ",
    'orderby' => 'datum.meta_value DESC',
    'limit' => 50
);

$objectList = pods('nyheter',$podparams);
$latestNewsID = 0;
$sideBarHtml = '';
$newsIndex = 0;

if ($objectList->total() > 0 ) {
    while ($objectList->fetch() ) {

        if($newsIndex<1){ $latestNewsID =  $objectList->field('slug'); }

        //  $linkArr = $objectList->field('puff_lank');
        $podpermalink = get_permalink($linkArr['ID']);

        $sideBarHtml .= '<a class="LeftMenuItem" href="'.$podpermalink.$objectList->field('slug').'">';
        $sideBarHtml .=  $objectList->field('name');
        $sideBarHtml .=  '</a>';
        $newsIndex++;
    }
}

if(isset($_GET['newsid'])){
    $latestNewsID = $_GET['newsid'];
}
?>

<div class="wrap container mainText PodsBrands subpage" role="document">


    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <?php get_template_part('templates/page', 'header'); ?>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-md-8 subContent">
            <!-- skriv ut sidan -->
            <?php get_template_part('templates/content', 'page'); ?>

            <?php


            $object = pods(  'nyheter', $latestNewsID );
            $html .='<span class="">';
            $html .='<h2 class="nyhetRubrik">'.$object->field('name').'</h2>';
            $html .='<span class="nyhetDatum">'.str_replace("00:00:00","", $object->field('datum') ).'</span>';
            $html .='<span class="nyhetText">'.$object->display('text').'</span>';
            $html .='</span>';



            echo $html;

            ?>
        </div>

        <div class="col-md-4 subBorderLeft">
            <div class="nyheter nyheter_lista" >
                <h3>Nyheter</h3>
                <?php
                echo $sideBarHtml;
                ?>
            </div>
        </div>
    </div>
</div>