<?php
/*
Template Name: Undersida Mall
*/

?>

<div class="wrap container mainText PodsBrands subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>
	</div>

	<div class="row relative minPageHeight">
		<div class="col-md-8 subContent">
			<?php get_template_part('templates/content', 'page'); ?>
		</div>

		<div class="col-md-4 subBorderLeft">
			<?php echo emitShowcases_vertical($cms); //exists in t1-lib.php ?>
		</div>
	</div>
</div>


