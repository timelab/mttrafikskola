<?php
/*
Template Name: Startsida Mall
*/
$site_url = get_template_directory_uri();

global $cms;
?>

	<!--[if IE 8]>
	<style>
		.flex-direction-nav .flex-prev
		{
			left: -20px;
			height: 118px;
			background-image: url(<?php $site_url; ?>/assets/img/pilvansterIE.png);
		}
		.flex-direction-nav .flex-next
		{
			right: -21px!important;
			height: 118px;
			background-image: url(<?php $site_url; ?>/assets/img/pilhogerIE.png);
		}
	</style>
	<![endif]-->

	<div class="container slider">
		<script>
			$(window).load(function()
			{
				$('.flexslider').flexslider(
					{
						animation: "slide",
						prevText: '',
						nextText: ''
					});
				$(".social").click(function()
				{
					$(".socialPanel").toggleClass('hideMe');
				});
			});
		</script>

		<div class="row">
			<div class="col-sm-12 no-padding">
				<?php
				$sliderId = get_post_meta(get_the_ID(), 'slider_id', true);
				if (!empty($sliderId))
				{
					echo emitSlider($cms, $sliderId);
				}
				?>
			</div>
		</div>
	</div>


<?php echo emitShowcases($cms); ?>


    <div class="wrap container mainText startpage" role="document">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <?php get_template_part('templates/page', 'header'); ?>
                <div class="page-text">
                    <?php get_template_part('templates/content', 'page'); ?>
                </div>
            </div>


        </div>
    </div>


<?php echo emitBrands($cms) ?>