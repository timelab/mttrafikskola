<?php
/*
Template Name: Galleriet-Enskilt
*/
 
$site_url = get_site_url();
$usedTags = "*";//str�ng med alla redan utskrivna taggar f�r att kunna kolla om dom �r utskrivna redan
$yearTagsString = ""; // denna fyller vi med alla knappar + text f�r alla �r
$regularTagsString = ""; // denna fyller vi med alla knappar + text f�r vanliga taggar

$album = array();
$years = array();

foreach ($cms->gallery->get_images_by_tag(get_post_meta(get_the_ID(), 'galleritagg', true)) as $picture)
{
	foreach ($picture->tags as $tag)
	{
		// echo "dev:".$tag."<BR>";
		if (is_numeric(trim($tag)))
		{
			// echo "numerisk tagg hitad!<BR>";
			if (str_replace(trim($tag),"" , $usedTags)==$usedTags) 
			{
			// sparar str�ng av knappar(�rtal) f�r senare utskrift;
				$usedTags = $usedTags.trim($tag) . "*";
				$yearTagsString = $yearTagsString . ' <div class="cbDiv "><input type="checkbox" class="cb-button" id="cb'.trim($tag).'"><span class="checkboxText">'.trim($tag).'</span></div>'; 
				
				array_push($years, trim($tag)); // fyller p� arrayen "years" med alla �rtal
			}
		} 
		else
		{
			if (str_replace(trim($tag), "",  $usedTags) == $usedTags) 
			{
				//   sparar str�ng av knappar(taggar/album) f�r senare utskrift;
				$usedTags = $usedTags.trim($tag) . "*";
				$regularTagsString = $regularTagsString . ' <div class="cbDiv fullWidth"><input type="checkbox" class="cb-button checkboxbtn" id="cb' . trim($tag).'" ><span class="checkboxText">' . str_replace(array("haltagning", "sagning", "-"),  array("h�ltagning", "s�gning", " ") ,$tag).'</span></div>'; 
				array_push($album, trim($tag)); // fyller p� arrayen album med alla taggar som inte �r �rtal
			}
		}
	}		
};         
?>

<script>

    
 $(function () {

$(".checkboxText").click(function (){
    //denna funktion �r f�r n�r man bara vill ha ETT album som kan vara aktivt
    $(".checkboxbtn").prop("checked", false);
    $(".checkboxText").removeClass("AlbumActive");
    $(this).addClass("AlbumActive");
    $(this).parent().find(".checkboxbtn").prop("checked", true);
    $(".thumbImgDiv").removeClass("HiddenDiv");
    $(".albumListan").addClass("HiddenDiv");
    $("#albumHeadline").text($(this).text());
    
});

//pita - kollar bilder i album och checkar i r�tt box
$(".albumListan>.thumbDiv").click(function (){
    $('#cb'+$(this).attr('id').replace('td_','')).click();
    $('#cb'+$(this).attr('id').replace('td_','')).parent().find(".checkboxText").click();
});

$(".cb-button, .checkboxText").click(function (){
        
	$(".thumbDiv").each(function () {
		$thisThumb = $(this);
		$thisImg = $("img", this);
		   
		if ($(!$(this).hasClass("thumbHidden"))) {$thisImg.addClass("thumbFade")}
	  // console.log("startar bygge av formel");
		if (<?php 

				// KOLLA �R
		 /*           echo '(';
					for($i = 0, $l = count($years); $i < $l; ++$i) {
						if ($i>0) {echo ' || ';};
						echo '($thisThumb.hasClass("thumb'.$years[$i].'") && $("#cb'.$years[$i].'").prop("checked"))'; 
						
					}
					echo ') || (';
						//denna tar hand om det inte finns n�got �rtal valt
						 for($i = 0, $l = count($years); $i < $l; ++$i) {
								if ($i>0) {echo ' && ';};
							   echo ' (!$("#cb'.$years[$i].'").prop("checked"))'; 
						}    
					echo ')) ';
		   */
	   
			
					//  $album  denna array skapas h�gre upp, och inneh�ller alla taggar som inte �r �rtal
		   //        echo '&&';
					echo '((';
				   for($i = 0, $l = count($album); $i < $l; ++$i) {
								if ($i>0) {echo ' || ';};
							   echo ' ($thisThumb.hasClass("thumb'.$album[$i].'") && $("#cb'.$album[$i].'").prop("checked"))'; 
				   };
				   echo ') || (';   
				   //denna for �r till f�r att ta hand om fallet d� INGET album �r valt
				   for($i = 0, $l = count($album); $i < $l; ++$i) {
								if ($i>0) {echo ' && ';};
							   echo ' (!$("#cb'.$album[$i].'").prop("checked"))'; 
				   }
				   
					echo '))';
				  
					
			 ?>){
				fadeIn($thisThumb, $thisImg);
			}else{
				fadeOut($thisThumb, $thisImg);
			}
							 
	  
	 });
});
 
 
function fadeIn($selectedDiv, $selectedImg){
    setTimeout(function(){ $selectedDiv.removeClass("thumbHidden");}, 500);
    setTimeout(function(){ $selectedImg.removeClass("thumbFade")}, 550);
}

function fadeOut($selectedDiv, $selectedImg){
    setTimeout(function(){$selectedDiv.addClass("thumbHidden");}, 500);
}


$(".thumbImg").click(function (){
    // �ppna den stora bilden
$thumb=$(this).attr("src");

    $("#blurr").css("opacity","0.90").css("height",$("html").height()).css("min-height",$(window).height());
    $(".imgBtnLeft").css("opacity","0.6").css("height","100%").css("width","80px");
    $(".imgBtnRight").css("opacity","0.6").css("height","100%").css("width","80px");
    
   
        
        
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv").removeClass("Prev");
   $(".thumbDiv").removeClass("Next");
   
   $(this).parent().addClass("Active");
   nextImg($('.thumbDiv.Active'));
   prevImg($('.thumbDiv.Active'));
    
  
       $("#showImg>img").css("opacity","0");
        setTimeout(  function()   {  
               $("#showImg>img").attr("src", "" );

               setTimeout(  function()   {
                   zoomIn();
                    
               }, 200);

        }, 100);
 
});

$(".imgBtnLeft").click(function (e){
  // console.log("prev");
   $("#showImg>img").attr("src", $(".thumbDiv.Prev>img").attr("src").replace("-150x150","") );
   
   $(".thumbDiv").removeClass("Next");
   $(".thumbDiv.Active").addClass("Next");
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv.Prev").addClass("Active");
   $(".thumbDiv").removeClass("Prev");
   prevImg($('.thumbDiv.Active')); 
   $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
   if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
   if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
rescale();
   e.preventDefault();
   e.stopPropagation();
})

$(".imgBtnRight").click(function (e){
   //console.log("next");
   $("#showImg>img").css("opacity","0");
   $("#showImg>img").attr("src", $(".thumbDiv.Next>img").attr("src").replace("-150x150","") );
   
   $(".thumbDiv").removeClass("Prev");
   $(".thumbDiv.Active").addClass("Prev");
   $(".thumbDiv").removeClass("Active");
   $(".thumbDiv.Next").addClass("Active");
   $(".thumbDiv").removeClass("Next");
   nextImg($('.thumbDiv.Active'));
   $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
   if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
   if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");}

 rescale();
   e.preventDefault();
   e.stopPropagation();
})

function prevImg(current){
        if(current.prev().length > 0){
            if(current.prev().hasClass('thumbHidden')){
                prevImg(current.prev());
            }else {
                
                current.prev().addClass('Prev');
            }
        }
}

function nextImg(current){
        if(current.next().length > 0){
            if(current.next().hasClass('thumbHidden')){
                nextImg(current.next());
            }else {
                current.next().addClass('Next');
            }
        }
        

}


$("#showImg, #blurr").click(function (){
    
  //st�nger ned bilden
    $(".thumbDiv").removeClass("Active");
    $(".thumbDiv").removeClass("Next");
    $(".thumbDiv").removeClass("Prev");
    
    $("#blurr").css("opacity","0").css("height","0%").css("min-height","0px");
  
    $("#showImg").css("margin-top","-2000px").css("opacity","0"); 
    
   
      $("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");
     $("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");
     $("#showImg>.imgText").css("opacity","0");
   
    setTimeout(  function()   {  $("#showImg>img").attr("src", "" );}, 100);
});


    
function zoomIn()
{
     $("#showImg img").css("height","");
     $("#showImg img").css("width","");
   //console.log("height och width borta");
     // klick p� thumbnail visar bilden
     $("#showImg").css("margin-top",$(document).scrollTop()-20).css("opacity","1");
     $("#showImg>img").attr("src", $thumb.replace("-150x150","") );
     
     $("#showImg>img").css("margin-top","0px").css("opacity","1");;
   
          
     $("#showImg>.imgBtnLeft").css("top","0px").css("left","0px").css("opacity","0.6");
     $("#showImg>.imgBtnRight").css("top","0px").css("right","0px").css("opacity","0.6");
     $("#showImg>.imgText").css("bottom","0px").css("opacity","1");
     
     $(".imgText").text($(".thumbDiv.Active>.pictureText").text());
     if($(".thumbDiv").hasClass("Prev")){$("#showImg>.imgBtnLeft").css("opacity","0.5").css("width","80px");}else{ $("#showImg>.imgBtnLeft").css("opacity","0").css("width","0px");}
     if($(".thumbDiv").hasClass("Next")){$("#showImg>.imgBtnRight").css("opacity","0.5").css("width","80px");}else{$("#showImg>.imgBtnRight").css("opacity","0").css("width","0px");}

     rescale();
        



     
}

function rescale(){
    $("#showImg img").css("height","");
    $("#showImg img").css("width","");
    $("#showImg img").css("opacity","0");
    setTimeout(  function()   {
   
            $("#showImg").width(img_width);
            
            // h�r r�knar vi ut bildens storlek n�r den ska visas
            var window_width=$(".container").width()-60;
            var window_height=$(window).height()-250;
            var img_width=$("#showImg img").width();
            var img_height=$("#showImg img").height();
            var img_ratio=0;
            
         //   console.log("IMG width:" + img_width + " / height:" + img_height);
         //   console.log("WIN width:" + window_width + " / height:" +window_height);
            
            img_ratio=window_width/img_width; // ratio f�r att procentuellt minska bilden i sidled
          //  console.log("asd");
          //  console.log("ratio:" +img_ratio); 
            // om bilden �r bredaren �n f�nster, s� skalar vi ned h�jd + bredd
            if (img_width>window_width){
           //         console.log("bild f�r bred");
                    img_height=img_height*img_ratio;
                    img_width=window_width;
                    img_height=img_height.toFixed(0);
            }
            img_ratio=window_height/img_height; // ratio f�r att procentuellt minska bilden i h�jdled
             //om bilden �r h�gre �n f�nstret, s� skalar vi ned h�jd+bredd
            if (img_height>window_height){
                
                 console.log("bild f�r h�g");
                 img_width=img_width*img_ratio;
                 img_height=window_height;
                 img_width=img_width.toFixed(0);
            }
            
          //  console.log("IMG width:" + img_width + " / height:" + img_height);
            
            $("#showImg img").height(img_height).width(img_width);
            $("#showImg").css("left","50%");
            $("#showImg").css("margin-left",(-1)*img_width/2);
            
            $("#showImg").width(img_width);
  
            
            
            var window_width=0;
            var window_height=0;
            var img_width=0;
            var img_height=0;
            var img_ratio=0;
            $("#showImg img").css("opacity","1");
        ;}, 200);
 
    
    
}


});
</script>
<style>
#showImg{ 
            transition: all 400ms;
            -moz-transition: all 400ms;
            -webkit-transition: all 400ms;
            -o-transition: all 400ms;
            z-index: 1000000100;
            cursor:hand; 
            cursor:pointer;
            margin: 0 auto;
            position: absolute; 
          
    }
    
#showImg .imgBtnLeft,  #showImg .imgBtnRight {
    opacity: 0 !important;
    -moz-transition: all 400ms;
    -webkit-transition: all 400ms;
    -o-transition: all 400ms; 
}
#showImg:hover .imgBtnLeft, #showImg:hover .imgBtnRight{
    opacity: 0.6 !important;
}   
    
    
 #blurr { position: absolute; width:100%; height:0%; min-height:0px; background-color:black; opacity:0.0; display:block; z-index: 1000000000; top:0px; left:0px;             transition: all 400ms;            -moz-transition: all 400ms;            -webkit-transition: all 400ms;            -o-transition: all 400ms;}   
  
    
#showImg>img{
   
    /*        transition: all 400ms;
            -moz-transition: all 400ms;
            -webkit-transition: all 400ms;
            -o-transition: all 400ms;*/
            position: relative;
            z-index: 2000;
            opacity:0;
            
            
     
    }
    .thumbDiv{cursor:hand; cursor:pointer; margin:2%; width: 28%; }
   
    .imgBtnLeft, .imgBtnRight, .imgText{
            width:0px; 
            height:100%;
            position: absolute; 
            z-index: 1000000200;
            display:block; 
            transition: all 400ms;
             -moz-transition: all 400ms;
            -webkit-transition: all 400ms;
            -o-transition: all 400ms;
            cursor:pointer; cursor:hand;
            background-color:black;
            opacity:0;
            
            
            
    }
    .imgBtnRight{right:0px;top:0px;top: -1000px;}
    .imgBtnLeft{left:0px; top:0px;top: -1000px;}
    .mainText>#showImg>.imgBtnRight:hover{opacity: 0.8 !important}
    .mainText>#showImg>.imgBtnLeft:hover{opacity: 0.8!important}

    
    .imgText{position: relative; bottom:-1000px; opacity:0; height:100px; width:100%; padding: 20px; color:white; overflow:hidden;}
    
    
    
    
    imgBtnRight i:before{
        width: 100%;
        text-align: center;
        top: 34%;
        position: relative;
        margin-top: -40px;
    }
 
    imgBtnLeft i:before{
        width: 100%;
        text-align: center;
        top: 34%;
        position: relative;
        margin-top: -40px;
    }    

    .thumbImgDiv {margin-top: 16px; }
    .HiddenDiv {display:none !important;}
    .thumbImg, .albumThumbImg {width:100%; }
    .thumbImgDiv div {transition: all 500ms; -moz-transition: all 500ms; -webkit-transition: all 500ms; -o-transition: all 500ms;} 
    .thumbImgDiv div img {transition: all 500ms; -moz-transition: all 500ms; -webkit-transition: all 500ms; -o-transition: all 500ms;} 

    
    .thumbFade{opacity:0;  margin: 0px;padding: 60px;}
    .thumbHidden{display:none;}

    .imgSearchDiv{padding: 0px 0px 0px 26px; font-size:14px; width:100%; display:block;}
    
    .cb-button {display:block; float:left !important;}
    .checkboxText {display:block; float: left; padding: 0px 31px 0px 3px; text-transform: capitalize; cursor:hand; cursor:pointer;}
    .cbDiv{display: block; float: left;}
    .fullWidth {width:100%}
    .cbHeadline{ font-size:24px; width:100%; float:left; display:block; text-transform: capitalize; margin-bottom: 20px;}
    
    .floatLeft{float:left;}
    .floatRight{float:right;}
    .pictureText{display:none;}
    
    .AlbumActive{ color:#9f0c1d; font-weight:bold;}  
    .checkboxbtn {display: none !important;}
    
    
    span.albumName {
        width: 100%;
        text-align: center;
        display: block;
        margin-top: 7px;
        text-transform: capitalize;
    }
    div.albumListan, div.thumbImgDiv {
        margin-left: -18px;
        margin-right: -21px;
    }
    
   
    #albumHeadline{margin-left: 18px; text-transform: capitalize; width:90%}
    .textWidth p{width:95%}
</style>  
<div id="blurr"></div>
<div class="wrap container mainText " role="document">
    <div id="showImg">
        <img src="">
        <span class="imgBtnLeft" ><i class="fa fa-angle-left  " style="margin-left: 22px;font-size: 79px;width: 100%;top: 40%;position: relative;"></i></span>
        <span class="imgBtnRight"><i class="fa fa-angle-right " style="margin-left: 22px;font-size: 79px;width: 100%;top: 40%;position: relative;"></i></span>
        <span class="imgText"></span>
    </div>
    
	<div class="row relative">
		<div class="subHeader">
			<div class="marginFixSub"><?php get_template_part('templates/page', 'header'); ?></div>
		</div>
		<div class="marginFixSub">
			<div class="col-sm-4 floatRight">
				<div class="row puffar imgSearchDiv ">
					<?php
						//h�r skrivs alla s�kalternativ ut.
						echo '<div class="cbHeadline">Album</div>';
						//echo ' <div class="cbDiv fullWidth"><input type="checkbox" class="cb-button checkboxbtn" id="cballabilder"><span class="checkboxText">Alla bilder</span></div>';
						echo ' <div class="cbDiv fullWidth"></div>';//<input type="checkbox" class="cb-button checkboxbtn" id="cballabilder"><span class="checkboxText">Alla bilder</span></div>';
						echo $regularTagsString;
					?>       
		</div>            
	</div> 

	<div class="col-sm-8 col-xs-12 pageTextArea pageTextNews floatLeft textWidth">
		<?php get_template_part('templates/content', 'page'); ?>
			<!-- --- ALBUM MED THUMBNAILS  -------------------------------------------- -->
		<div  class="albumListan">
			<?php   
				//loopar igenom alla bilder f�r att hitta f�rsta bilden f�r varje album            
				
				//foreach ($cms->gallery->get_images() as $picture)
				foreach ($cms->gallery->get_images_by_tag(get_post_meta(get_the_ID(), 'galleritagg', true)) as $picture)
				{ 
					$imageTags = $picture->post_excerpt; // alla taggar f�r vald bild
					$imageTags .= " ";
					$i = 1;
					do
					{
						$i++;
						$selectedAlbum=substr($imageTags, 1, strpos($imageTags," " )); // plocka ut f�rsta taggen
					 
						if ($selectedAlbum == "") { break; }
						//h�r kollar vi om vi redan visat denna tagg, is�fall t�mmer vi $selectedAlbum
						if (strpos($usedTags,trim($selectedAlbum)) == true) 
						{
							$imageTags = ""; // vi t�mmer resten, vi har hittat en match
							$usedTags = str_replace(trim($selectedAlbum), "", $usedTags);
							$imageTags = "";
						}
						$imageTags = str_replace($selectedAlbum, '', $imageTags );//ta bort den utplockade taggen
						
						if ($i > 10) 
						{ 
							echo "<BR>loopat 10ggr<BR>"; 
							break; 
						}
					}
					while (strlen(str_replace('*', '', $imageTags)) > 0);
					
					if ($selectedAlbum != "") 
					{
						echo '<div id="td_' . trim($selectedAlbum) . '" class="col-sm-4 col-xs-6 thumbDiv '; 
						echo $selectedAlbum;
						echo '"><img src="' . str_replace(".jpg", "-150x150.jpg", $picture->guid) . '" class="albumThumbImg">';
						echo '<span class="albumName"> ' . str_replace(array("haltagning", "sagning", "-"),  array("h�ltagning", "s�gning", " "), $selectedAlbum) . '</span></div>';
						$usedAlbums .= $selectedAlbum . "*";
					}
				}
			?>        
	</div>
	<!-- ----------------------------------------------------------- -->
				
	<div class="row thumbImgDiv HiddenDiv">
		<div><h2 id="albumHeadline">Rubrik</h2></div>
		<?php         
			foreach ($cms->gallery->get_images() as $picture)
			{
				echo '<div class="col-sm-4 col-xs-6 thumbDiv '; 
				echo str_replace("#", "thumb", $picture->post_excerpt);
				echo '"><img src="' . str_replace(".jpg", "-150x150.jpg", $picture->guid) . '" class="thumbImg">';
				echo '<span class="pictureText"> ' . $picture->post_content . '</span></div>';
			   // echo var_dump($picture);
			}
		?>        
		</div>
	</div>
			
			

	</div>
	</div>
</div>

