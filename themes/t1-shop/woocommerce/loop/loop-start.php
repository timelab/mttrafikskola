<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

?>
<div class="clearfix"></div>
<div class="products <?php echo is_product_category() ? '':'row' ?>">