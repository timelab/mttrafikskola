
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
  <div class="container">
      <div class="row top-header">
		  <?php do_action('icl_language_selector'); ?>
          <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6">
              <img src="<?php echo $cms->getApi('Logo')->getLogo(); ?>" alt="Logotype" class="topLogo"/>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
              <div class="row">
                  
                  
                  
                  <?php
				  global $cms;



        		?>
    
                    
                  
                  
                  
                  
                  <div class="col-sm-6 hidden-xs topInfo">
                    <?php echo $contactBtn; ?>
                      
                      
                  </div>
                  <div class="col-sm-4 social">
					  <?php
					  global $woocommerce;

					  //if(is_shop() || is_product_category() || is_product() || $woocommerce->cart->cart_contents_count > 0){?>
						  <!--<div class="cart-contents-container">-->
						  <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('Visa kundkorg', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo $woocommerce->cart->cart_contents_count;?></span></a>
						  <!--</div>-->
					  <?php
					  //}
					  ?>

					  <a href="http://instagram.com/sambolulea" target="_blank"><i class="fa fa-instagram"></i></a>
					  <a href="https://www.facebook.com/sambostorheden" target="_blank"><i class="fa fa-facebook"></i></a>


                  </div>
              </div>
          </div>
      </div>
    <?php
      do_action('get_header');
      // Use Bootstrap's navbar if enabled in config.php
     if (current_theme_supports('bootstrap-top-navbar')) {
        get_template_part('templates/header-top-navbar');
      } else {
        get_template_part('templates/header');
      }
    ?>
  </div>

  
<?php include roots_template_path(); ?>


<?php get_template_part('templates/footer'); ?>
</body>
</html>
