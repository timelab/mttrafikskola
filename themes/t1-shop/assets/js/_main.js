// Modified http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
// Only fires on body class (working off strictly WordPress body_class)

var ExampleSite = {
  // All pages
  common: {
    init: function() {
      // JS here

        //Facebook
        $('.facebookBtn').click(function(){
            toggleSocial($('div.facebook'));
        });

        $('.instagramBtn').click(function(){
            toggleSocial($('div.instagram'));
        });

        $('.socialPane .socialClose').click(function(){
            $(this).parent().parent().removeClass('open');
        });

        function toggleSocial($target){
            if($target.hasClass('open')){
                $target.removeClass('open');
            }else{

                //Stäng ev. öppna panels
                $('.socialPane.open').removeClass('open');

                //Öppna denna
                $target.addClass('open');
            }
        }

      //Lägg till en clone av huvudmenyval när det finns dropdowns
      //Loopa alla a med klassen dropdown-toggle
      $('.navbar-nav a.dropdown-toggle').each(function(){
        //Clona first-child i submeny (Borde alltid finnas åtminstone en eftersom det är en dropdown)
       // $cloned = $('ul li:first-child',$(this).parent()).clone();

        //Ta klasserna från huvudmenyval men exkludera dropdownklassen
        var $classes = $(this).parent().attr('class').replace('dropdown','');

        //Om det finns en annan submeny som är aktiv, ta bort även active classen som kommer från huvudmenyval då en submeny är active
        $classes = $(this).parent().find('ul li.active').length > 0 ? $classes.replace('active', ''):$classes;

        //Modifiera classer,href och text på clonen för att spegla huvudmenyval
        $cloned.attr('class',$classes).find('a').attr('href',$(this).attr('href')).text($(this).text());

        //Prependa den så den hamnar först i submenyn
        $('ul',$(this).parent()).prepend($cloned);
      });


      if($('.attachment-shop_catalog.wp-post-image').length > 0){
        $('.attachment-shop_catalog.wp-post-image').each(function(){
         $(this).wrap($('<div />',{'class':'product-list-img-wrapper'}));
        });

      }
    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JS here
    }
  },
  // About page
  about: {
    init: function() {
      // JS here
    }
  },

  woocommerce_page: {
    init: function() {
        $(".nav.navbar-nav .dropdown.menu-butik").addClass("active");
    }
  }
};

var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = ExampleSite;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {

    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });

    UTIL.fire('common', 'finalize');
  }
};

$(document).ready(UTIL.loadEvents);
