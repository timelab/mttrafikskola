'use strict';
module.exports = function(grunt) 
{
	grunt.initConfig(
	{
	    less: 
	    {
	    	production: 
	    	{
	    		options: 
	    		{
	    			cleancss: true,
	    			compress: true
	    		},
	    		files: 
	    		{
	    			'assets/css/main.min.css': 
	    			[
	    			 	'assets/less/app.less'
	    			]
	    		}
	    	}
	    },
	    uglify: 
	    {
	    	dist: 
	    	{
	    		files: 
	    		{
	    			'assets/js/scripts.min.js': 
	    			[
                        'assets/js/plugins/bootstrap/tooltip.js',
                        'assets/js/plugins/bootstrap/*.js',
			            'assets/js/_main.js',
			            'assets/js/_*.js',
			            'assets/js/plugins/_*.js'
		            ]
	    		}
	    	}
	    },
	    watch: 
	    {
	    	less: 
	    	{
	    		files: 
	    		[
	    		 	'assets/less/*.less',
	    		 	'assets/less/templates/*.less',
	    		 	'assets/less/page-templates/*.less',
	    		 	'assets/less/bootstrap/*.less'
	    		],
	    		tasks: ['less']
	    	},
	    	js: 
	    	{
	    		files: 
	    		[
					'Gruntfile.js',
					'assets/js/*.js',
					'assets/js/plugins/*.js',
					'!assets/js/scripts.min.js'
	    		],
	    		tasks: ['uglify']
	    	}
		},
	    clean: 
	    {
	    	dist: 
	    	[
	    	 	'assets/css/main.min.css',
	    	 	'assets/js/scripts.min.js'
	    	]
		}
	});

	
	// Load tasks
	//grunt.loadTasks('tasks');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Register tasks
	grunt.registerTask('default', 
	[
	    'less',
	    'uglify'
    ]);
	grunt.registerTask('dev', 
	[
	 	'watch'
	]);
};
