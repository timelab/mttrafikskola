<?php
global $query_string;




$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
}


$search = new WP_Query($search_query);

?>

<div class="wrap container mainText PodsBrands subpage woocommerce-page" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<div class="page-header">
				<h1>Sökresultatet</h1>
			</div>
		</div>
	</div>

	<div class="row relative minPageHeight">
		<div class="col-sm-9 col-md-8 subContent">
			<?php
			if(get_post_type() == '') {
				echo "Tyvärr gav er sökning inga resultat.";
			}
			?>
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('templates/content', 'search-'.get_post_type()); ?>
			<?php endwhile; ?>
            <?php do_action( 'woocommerce_after_shop_loop' ); ?>
		</div>

		<div class="col-sm-3 col-md-4 subBorderLeft shop-sidebar">
			<div class="view-btn"><i class="fa fa-spinner fa-spin"></i></div>
			<div class="row showcases collapsed">
				<div class="col-sm-12 shop-cat-menu">
					<ul class="widget-ul">
						<?php
						dynamic_sidebar('Shop Sidebar');
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>